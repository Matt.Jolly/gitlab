## GitLab configuration settings
##! This file is generated during initial installation and **is not** modified
##! during upgrades.
##! Check out the latest version of this file to know about the different
##! settings that can be configured, when they were introduced and why:
##! https://gitlab.com/gitlab-org/omnibus-gitlab/blame/master/files/gitlab-config-template/gitlab.rb.template

##! Locally, the complete template corresponding to the installed version can be found at:
##! /opt/gitlab/etc/gitlab.rb.template

##! You can run `gitlab-ctl diff-config` to compare the contents of the current gitlab.rb with
##! the gitlab.rb.template from the currently running version.

##! You can run `gitlab-ctl show-config` to display the configuration that will be generated by
##! running `gitlab-ctl reconfigure`

##! In general, the values specified here should reflect what the default value of the attribute will be.
##! There are instances where this behavior is not possible or desired. For example, when providing passwords,
##! or connecting to third party services.
##! In those instances, we endeavour to provide an example configuration.

## GitLab URL
##! URL on which GitLab will be reachable.
##! For more details on configuring external_url see:
##! https://docs.gitlab.com/omnibus/settings/configuration.html#configuring-the-external-url-for-gitlab
##!
##! Note: During installation/upgrades, the value of the environment variable
##! EXTERNAL_URL will be used to populate/replace this value.
##! On AWS EC2 instances, we also attempt to fetch the public hostname/IP
##! address from AWS. For more details, see:
##! https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instancedata-data-retrieval.html
external_url 'https://gitlab.lab.footclan.ninja'

################################################################################
################################################################################
##                Configuration Settings for GitLab CE and EE                 ##
################################################################################
################################################################################

################################################################################
## gitlab.yml configuration
##! Docs: https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/settings/gitlab.yml.md
################################################################################
# gitlab_rails['gitlab_ssh_host'] = 'ssh.host_example.com'
# gitlab_rails['gitlab_ssh_user'] = ''
gitlab_rails['time_zone'] = 'Australia/Sydney'

### Request duration
###! Tells the rails application how long it has to complete a request
###! This value needs to be lower than the worker timeout set in unicorn/puma.
###! By default, we'll allow 95% of the the worker timeout
gitlab_rails['max_request_duration_seconds'] = 175

### GitLab email server settings
###! Docs: https://docs.gitlab.com/omnibus/settings/smtp.html
###! **Use smtp instead of sendmail/postfix.**

gitlab_rails['smtp_enable'] = false
gitlab_rails['smtp_address'] = "mail.footclan.ninja"
gitlab_rails['smtp_port'] = 25
gitlab_rails['smtp_domain'] = "footclan.ninja"
gitlab_rails['smtp_authenticaton'] = false
gitlab_rails['smtp_enable_starttls_auto'] = true
gitlab_rails['smtp_tls'] = false

###! **Can be: 'none', 'peer', 'client_once', 'fail_if_no_peer_cert'**
###! Docs: http://api.rubyonrails.org/classes/ActionMailer/Base.html
gitlab_rails['smtp_openssl_verify_mode'] = 'none'

### Email Settings

gitlab_rails['gitlab_email_enabled'] = true

##! If your SMTP server does not like the default 'From: gitlab@gitlab.example.com'
##! can change the 'From' with this setting.
gitlab_rails['gitlab_email_from'] = 'gitlab@footclan.ninja'
gitlab_rails['gitlab_email_display_name'] = 'GitLab'

### GitLab user privileges
gitlab_rails['gitlab_username_changing_enabled'] = true

### Consolidated (simplified) object storage configuration
###! This uses a single credential for object storage with multiple buckets.
###! It also enables Workhorse to upload files directly with its own S3 client
###! instead of using pre-signed URLs.
###!
###! This configuration will only take effect if the object_store
###! sections are not defined within the types. For example, enabling
###! gitlab_rails['artifacts_object_store_enabled'] or
###! gitlab_rails['lfs_object_store_enabled'] will prevent the
###! consolidated settings from being used.
###!
###! Be sure to use different buckets for each type of object.
###! Docs: https://docs.gitlab.com/ee/administration/object_storage.html
gitlab_rails['object_store']['enabled'] = true
gitlab_rails['object_store']['direct_upload'] = true
gitlab_rails['object_store']['connection'] = {
  'provider' => 'AWS',
  'region' => 'ap-southeast-2',
  'aws_access_key_id' => 'accesskey_goes_here',
  'aws_secret_access_key' => 'secretkey_goes_here',
  # # The below options configure an S3 compatible host instead of AWS
  # 'aws_signature_version' => 4, # For creation of signed URLs. Set to 2 if provider does not support v4.
  'endpoint' => 'https://api.minio.lab.footclan.ninja', # default: nil - Useful for S3 compliant services such as DigitalOcean Spaces
  'path_style' => true # Use 'host/bucket_name/object' instead of 'bucket_name.host/object'
}

gitlab_rails['object_store']['objects']['artifacts']['bucket'] = "gitlab-artifacts-storage"
gitlab_rails['object_store']['objects']['external_diffs']['bucket'] = nil
gitlab_rails['object_store']['objects']['lfs']['bucket'] = "gitlab-lfs-storage"
gitlab_rails['object_store']['objects']['uploads']['bucket'] = "gitlab-uploads-storage"
gitlab_rails['object_store']['objects']['packages']['bucket'] = "gitlab-packages-storage"
gitlab_rails['object_store']['objects']['dependency_proxy']['bucket'] = "gitlab-dependency-proxy"
gitlab_rails['object_store']['objects']['terraform_state']['bucket'] = "gitlab-terraform-state"
gitlab_rails['object_store']['objects']['pages']['bucket'] = "gitlab-pages-storage"



### LDAP Settings
###! Docs: https://docs.gitlab.com/omnibus/settings/ldap.html
###! **Be careful not to break the indentation in the ldap_servers block. It is
###!   in yaml format and the spaces must be retained. Using tabs will not work.**

gitlab_rails['ldap_enabled'] = false
gitlab_rails['prevent_ldap_sign_in'] = false

###! **remember to close this block with 'EOS' below**
gitlab_rails['ldap_servers'] = {
  'main' => { # 'main' is the GitLab 'provider ID' of this LDAP server
    'label' => 'LAB', 
    'host' => 'lab.footclan.ninja',
    'port' => '389',
    'uid' => 'sAMAccountName',
    'bind_dn' => 'DN of your LDAP Bind Account',
    'password' => 'Password for your LDAP Bind account',
    'encryption' => 'plain',# "start_tls" or "simple_tls" or "plain"
    'verify_certificates' => false,
    'smartcard_auth' => false,
    'active_directory' =>  true,
    'allow_username_or_email_login' => false,
    'lowercase_usernames' => false,
    'block_auto_created_users' => false,
    'base' => 'dc=lab,dc=footclan,dc=ninja-',
    'user_filter' => '',
    'attributes' => {
      'username' => ['uid', 'userid', 'sAMAccountName'],
      'email' => ['mail', 'email', 'userPrincipalName'],
      'name' => 'cn',
      'first_name' => 'givenName',
      'last_name' => 'sn'
    }
    ## EE only
#     group_base: ''
#     admin_group: ''
#     sync_ssh_keys: false
#
  }
}
#gitlab_rails['omniauth_allow_single_sign_on'] = ['github']
#gitlab_rails['omniauth_auto_link_ldap_user'] = true 
#gitlab_rails['omniauth_auto_link_user'] = ['github']
#gitlab_rails['omniauth_providers'] = [
#   {
#     "name" => "github",
#     "app_id" => "APP_ID",
#     "app_secret" => "APP_SECRET",
#     "url" => "https://your.github.enterprise.URI/",
#     "verify_ssl" => false,
#    "args" => { "scope" => "user:email" }
#   }
#]

################################################################################
## Container Registry settings
##! Docs: https://docs.gitlab.com/ee/administration/container_registry.html
################################################################################

registry_external_url 'https://registry.gitlab.lab.footclan.ninja'
gitlab_rails['registry_enabled'] = true
gitlab_rails['api_url'] = 'https://registry.gitlab.lab.footclan.ninja'
registry['enable'] = true
registry_nginx['enable'] = false
registry['registry_http_addr'] = "0.0.0.0:5000"
registry['health_storagedriver_enabled'] = false
registry_nginx['proxy_set_headers'] = {"X-Forwarded-Proto" => "https","X-Forwarded-Ssl" => "on"}

###! Docs: https://docs.gitlab.com/ee/administration/packages/container_registry.html#configure-storage-for-the-container-registry
registry['storage'] = {
  's3' => {
    'accesskey' => 'accesskey_goes_here',
    'secretkey' => 'secretkey_goes_here',
    'bucket' => 'gitlab-registry-storage',
    'region' => 'us-east-1',
    'regionendpoint' => 'S3_endpoint_goes_here',
#    'secure' => true,
#    'encrypt' => false,
    'v4Auth' => true
  }
}

################################################################################
## GitLab Workhorse
##! Docs: https://gitlab.com/gitlab-org/gitlab-workhorse/blob/master/README.md
################################################################################

gitlab_workhorse['proxy_headers_timeout'] = "5m0s"

################################################################################
## GitLab Puma
##! Tweak puma settings. You should only use Unicorn or Puma, not both.
##! Docs: https://docs.gitlab.com/omnibus/settings/puma.html
################################################################################

puma['worker_timeout'] = 180

# Let long-running jobs run without timing out
# Seconds (?)`
gitlab_rails['env'] = {
   'GITLAB_RAILS_RACK_TIMEOUT' => 600
}

################################################################
## GitLab PostgreSQL
################################################################

postgresql['statement_timeout'] = "160000"
postgresql['idle_in_transaction_session_timeout'] = "160000"

################################################################################
## GitLab Redis
##! **Can be disabled if you are using your own Redis instance.**
##! Docs: https://docs.gitlab.com/omnibus/settings/redis.html
################################################################################

redis['tcp_timeout'] = "160"

################################################################################
## GitLab Web server
##! Docs: https://docs.gitlab.com/omnibus/settings/nginx.html#using-a-non-bundled-web-server
################################################################################

##! When bundled nginx is disabled we need to add the external webserver user to
##! the GitLab webserver group.
# web_server['external_users'] = []
# web_server['username'] = 'gitlab-www'
# web_server['group'] = 'gitlab-www'
# web_server['uid'] = nil
# web_server['gid'] = nil
# web_server['shell'] = '/bin/false'
# web_server['home'] = '/var/opt/gitlab/nginx'

################################################################################
## GitLab NGINX
##! Docs: https://docs.gitlab.com/omnibus/settings/nginx.html
################################################################################

nginx['redirect_http_to_https'] = false # Edge Router terminates our SSL.
nginx['listen_port'] = 8181 # Arbitrary port
##! Docs: https://docs.gitlab.com/omnibus/settings/nginx.html#supporting-proxied-ssl
nginx['listen_https'] = false

# Forward headers to make GitLab work properly.
nginx['proxy_set_headers'] = {
  "X-Forwarded-Proto" => "https",
  "X-Forwarded-Ssl" => "on"
}

# This configuration block tells GitLab to suppress the 10.0.0.x range (docker internal network)
# Be sure to update it for your ingress network (traefik-network) IP range (docker network inspect traefik-network)
# We use the X-Real-IP header added by Traefik to provide useful logs!
nginx['real_ip_trusted_addresses'] = [ "10.0.0.0/24" ]
nginx['real_ip_header'] = 'X-Real-IP'
nginx['real_ip_recursive'] = 'on'

################################################################################
## Git
##! Advanced setting for configuring git system settings for omnibus-gitlab
##! internal git
################################################################################

##! For multiple options under one header use array of comma separated values,
##! eg.:
##! { "receive" => ["fsckObjects = true"], "alias" => ["st = status", "co = checkout"] }

 omnibus_gitconfig['system'] = {
#  "pack" => ["threads = 1", "useSparse = true"],
#  "receive" => ["fsckObjects = true", "advertisePushOptions = true"],
#  "repack" => ["writeBitmaps = true"],
#  "transfer" => ["hideRefs=^refs/tmp/", "hideRefs=^refs/keep-around/", "hideRefs=^refs/remotes/"],
#  "core" => [
#    'alternateRefsCommand="exit 0 #"',
#    "fsyncObjectFiles = true"
#  ],
#  "fetch" => ["writeCommitGraph = true"]
   "http" => ["sslVerify = false"]
 }

################################################################################
## GitLab Mattermost
##! Docs: https://docs.gitlab.com/omnibus/gitlab-mattermost
################################################################################

mattermost['enable'] = false

###############################################################################
# LetsEncrypt
################################################################################
letsencrypt['enable'] = false

################################################################################
## Dependency proxy (EE Only)
##! Docs: https://docs.gitlab.com/ee/administration/dependency_proxy.md
################################################################################

gitlab_rails['dependency_proxy_enabled'] = true
gitlab_rails['dependency_proxy_storage_path'] = "/var/opt/gitlab/gitlab-rails/shared/dependency_proxy"
#gitlab_rails['dependency_proxy_object_store_enabled'] = true
#gitlab_rails['dependency_proxy_object_store_direct_upload'] = false
#gitlab_rails['dependency_proxy_object_store_background_upload'] = true
#gitlab_rails['dependency_proxy_object_store_proxy_download'] = false
#gitlab_rails['dependency_proxy_object_store_remote_directory'] = "gitlab-dependency-proxy"
# gitlab_rails['dependency_proxy_object_store_connection'] = {
#   'provider' => 'AWS',
#   'region' => 'eu-west-1',
#   'aws_access_key_id' => 'AWS_ACCESS_KEY_ID',
#   'aws_secret_access_key' => 'AWS_SECRET_ACCESS_KEY',
#   # # The below options configure an S3 compatible host instead of AWS
#   # 'host' => 's3.amazonaws.com',
#   # 'aws_signature_version' => 4, # For creation of signed URLs. Set to 2 if provider does not support v4.
#   # 'endpoint' => 'https://s3.amazonaws.com', # default: nil - Useful for S3 compliant services such as DigitalOcean Spaces
#   # 'path_style' => false # Use 'host/bucket_name/object' instead of 'bucket_name.host/object'
# }

################################################################################
# Pgbouncer (EE only)
# See [GitLab PgBouncer documentation](http://docs.gitlab.com/omnibus/settings/database.html#enabling-pgbouncer-ee-only)
# See the [PgBouncer page](https://pgbouncer.github.io/config.html) for details
################################################################################
pgbouncer['client_login_timeout'] = 120

#########################################
# Gitlab Pages
# https://docs.gitlab.com/ee/administration/pages/
#########################################

pages_external_url 'https://pages.gitlab.lab.footclan.ninja'
pages_nginx['listen_https'] = false
pages_nginx['proxy_set_headers'] = {"X-Forwarded-Proto" => "https","X-Forwarded-Ssl" => "on"}
gitlab_pages['enable'] = true
gitlab_pages['inplace_chroot'] = true # Required because we're in docker
gitlab_pages['access_control'] = true # Restrict pages to those with permission to view the project
gitlab_pages['propagate_correlation_id'] = true # Passes through 'X-Request-ID' values to enable request correlation in the logs.
pages_nginx['listen_port'] = 5200

